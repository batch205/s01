let students = [
    "John",
    "Joe",
    "Jane",
    "Jessie"

];

console.log(students);

class Dog {
    constructor(name, breed, age){

        this.name = name;
        this.breed = breed;
        this.age = age;

    }

}

let dog1 = new Dog("Bantay", "corgi",3);

console.log(dog1);

let person1 ={

    name: "Saitama",
    heroName: "One Punch Man",
    age: 30

};

console.log(person1);

//What array method can we use to add an item at the end of an array?
students.push("Henry");
console.log(students);

//What array method can we use to add an item at the start of an array?

//students.unshift("Jeremiah");
console.log(students);

students.unshift("Christine");
console.log(students);

//What array method does the opposite of push()?

students.pop("Christine");
console.log(students);

//What array method does the opposite of unshift()?

students.shift("Jeremiah");
console.log(students);

//What is the difference between splice() and slice?
// .splice() allows to remove and add items from a starting index.(Mutator)
// .slice() copies a portion from a starting index and returns a new array from it.(Non-Mutator)

//what is another kind of array method?
//Iterator methods loops over the items of an array.

//forEach() - loops over items in an array and repeats a user-defined function.
//map() - loops over items in an array and repeats a user-defined function AND returns a new array. 

//every() - loops and checks if all items satisfy a given condition and returns a boolean.
let allDivisible;
let arrNum = [15,25,50,20,10,11];

//check if every item in arrNum is divisible by 5:
arrNum.forEach(num => {

    if (num % 5 === 0) {
        console.log(`${num} is divisible by 5.`)    
    }else{
        allDivisible = false;
    } 

    //However, can forEach() return data that will tell us IF all numbers/ items in our arrNum is divisible by 5?
})

console.log(allDivisible);  

/* arrNum.pop(11);
arrNum.push(35); */
let divisibleBy5 = arrNum.every(num => {

    //Every is able to return data.
    //When the function inside every() s able to return true, he loop will continue until all items are able to return true. Else, IF at least one item returns false, then the loop will stop there and every() will return false.

    console.log(num);
    return num% 5 === 0;

})
console.log(divisibleBy5);

//some() - loops and checks if at least one item satisfies a given condition and returns a boolean. It will true if at least ONE item satisfies the condition, else IF no item returns true/satisfies the condition, some() will return false.

let someDivisibleBy5 = arrNum.some (num =>{

    //some() will stop its loop once a function/item is able to return true.
    //Then, some() will return true else, if all items does not satisfy the condition or return true, some() will return false().

    console.log(num);
    return num % 5 === 0;

})
console.log(someDivisibleBy5);

arrNum.push(35);
let someDivisibleBy6 = arrNum.some(num => {

    console.log(num);
    return num % 6 === 0;
});

console.log(someDivisibleBy6);

//ACTIVITY - QUIZ
/* 
1. How do you create arrays in JS?
   Answer: using []

2. How do you access the first character of an array?
   Answer: using arrayName.lastIndexOf() method    

3. How do you access the last character of an array?
   Answer: using .lastIndexOf() method 

4. What array method searches for, and returns the index of a given in an array?
    Answer: .indexOf() method
    a. This method returns- 1 if given value is not found in the array.
    Answer: using .lastIndexOf() method 

5. What array method loops over all elements of an array, performing a user-defined function on each iteration?
    Answer: forEach() method

6. What array method creates a new array with elements/ data returned from a user-defined function?
    Answer: map() method

7. What array method checks if all its elements satisfy a given condition?
    Answer: every() method

8. What array method checks if at least one of its elements satisfies a given condition?
    Answer: some() method

9. True or False: array.splice() modifies  a copy of the array, leaving the original unchanged.
    Answer: True

10. True of False: array.slice() copies elements from original array and returns these as a new array.
    Answer: True

11. An Array is a primitive data type. True of False?
    Answer: True

12. What global JS object contains pre-defined methods and properties that are used for mathematical calculations?
    Answer: Math

13. True or False? The number of characters in a string can be determined using the .length property because strings are actually objects too.
    Answer: True

14. True or False? The methods of an Array are user-defined.
    Answer: False

15. True or False? We can use the.toString() on a Boolean
    Answer: True


*/

//FUNCTION CODING SECTION

//1. Create a function named addtoEnd that will add a passed in string to the end of a passed in array. If element to be added is not a string, return string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Ryan" as arguments when testing.

let addToEnd = (students => {
  students.push("Ryan")
    
   
    if (addToEnd != "") {
      return("error - can only add strings to an array ")
    }else{
        console.log(students);   
   
    } 

});

//2. Create a function named addToStart that will add a passed in string to the start of apassed in array. If element to be added is not a string, return the string" error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Tess" as arguments when testing.


let addToStart = (students => {
    students.push("Ryan")
    students.unshift("Tess")
    
    if (students === "" | students != "") {
      return "error - can only add strings to an array "
    }else{
        console.log(students);   
   
    } 

});

//3. Create a function named elementChecker that will check a passed in array if atleast one of its elements has the same value as a passed in argument. If array is empty, return the message "error - passed in array is empty". Otherwise, return a boolean value depending on the result of the check. Use the students array and the string "Jane" as arguments when testing.

let elementChecker = (students =>{
    students.push("Ryan")
    students.unshift("Tess")
    if (students === 'Jane'| students === null) {
        return "error - passed in array is empty "   
      }else{
        console.log(true);   
     
      } 
  
})

//4. Create a function named string LengthSorter that will take in an array of strings as its argument and sort its eleents in an ascending order based on their lengths. If at least one element is not a string, return "error - all array elements must be strings". Othewise, return the sorted array. Use the students array to test.

let stringLengthSorter = (students => {
    students.push("Ryan")
    students.unshift("Tess")
    students.sort()
    
    if (students == "Tess") {
      console.log(students)
    }else{
        return "error - all array elements must be strings ";   
   
    } 

});
